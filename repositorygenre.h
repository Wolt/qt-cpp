#ifndef REPOSITORYGENRE_H
#define REPOSITORYGENRE_H

#include <qsqldatabase.h>
#include <qsqlquerymodel.h>

class RepositoryGenre {
public:
    RepositoryGenre();
    QSqlQueryModel *getAll();
    bool deleteById(int id);
    int insert(QString name);
};

#endif // REPOSITORYGENRE_H
