#include "mainwindow.h"
#include "sqllite.h"
#include "ui_mainwindow.h"
#include "genredialog.h"
#include "albumdialog.h"

#include <QDialog>
#include <QMouseEvent>
#include <QSqlQueryModel>
#include <QStandardItemModel>
#include <qpixmap.h>
#include <qsqlrecord.h>

QString DEFAULT_ALBUM_LABEL_SPLITTER = ">>";
QString DATABASE_FILE_NAME = "data.db";

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow) {
    this->ui->setupUi(this);
}

MainWindow::~MainWindow() {
    delete this->repositoryGenre;
    delete this->repositoryAlbum;
    delete this->repositoryAuthor;
    delete this->ui;
}

bool MainWindow::setup(bool createTables, bool populateTables) {
    bool success = this->setupDatabase(createTables, populateTables);
    if(!success) {
        return false;
    }

    setupUi();

    return true;
}

bool MainWindow::setupDatabase(bool createTables, bool populateTables) {
    SqlLiteDatabase database = SqlLiteDatabase(DATABASE_FILE_NAME);
    if(createTables) {
        bool success = database.createTables();
        if(!success) {
            return false;
        }
    }

    if(populateTables) {
        bool success = database.populateTables();
        if(!success) {
            return false;
        }
    }

    this->repositoryGenre = new RepositoryGenre();
    this->repositoryAlbum = new RepositoryAlbum();
    this->repositoryAuthor = new RepositoryAuthor();
    return true;
}

void MainWindow::setupUi() {
    this->m_listViewMode = ALBUM_MODE;
    this->m_maximized = false;
    this->setAlbumName();

    QSqlQueryModel *genresModel = this->repositoryGenre->getAll();
    this->ui->listViewGenres->setModel(genresModel);
    this->ui->listViewGenres->setModelColumn(GENRE_NAME);

    QStandardItemModel *albumsModel = this->repositoryAlbum->getAll();
    this->ui->listViewAlbums->setModel(albumsModel);

    QObject::connect(this->ui->closeButton, &QPushButton::clicked, this, &MainWindow::closeClicked);
    QObject::connect(this->ui->minimizeButton, &QPushButton::clicked, this, &MainWindow::minimizeClicked);
    QObject::connect(this->ui->maximizeButton, &QPushButton::clicked, this, &MainWindow::maximizeClicked);

    QObject::connect(this->ui->listViewAlbums, &QAbstractItemView::doubleClicked, this, &MainWindow::albumDoubleClicked);

    QObject::connect(this->ui->pushButtonAlbums, &QPushButton::clicked, this, &MainWindow::albumNavigationClicked);
    QObject::connect(this->ui->pushButtonAddAlbum, &QPushButton::clicked, this, &MainWindow::addAlbumClicked);
    QObject::connect(this->ui->pushButtonDeleteAlbum, &QPushButton::clicked, this, &MainWindow::deleteAlbumClicked);

    QObject::connect(this->ui->pushButtonAddGenre, &QPushButton::clicked, this, &MainWindow::addGenreClicked);
    QObject::connect(this->ui->pushButtonDeleteGenre, &QPushButton::clicked, this, &MainWindow::deleteGenreClicked);

    QObject::connect(this->ui->pushButtonSearch, &QPushButton::clicked, this, &MainWindow::searchClicked);
}

void MainWindow::albumDoubleClicked() {
    qDebug() << "Album double clicked :)";
    if(this->m_listViewMode == ALBUM_MODE) {
        this->m_listViewMode = ALBUM_SONGS_MODE;
        QStandardItemModel *albumsModel = (QStandardItemModel*)this->ui->listViewAlbums->model();

    //    qDebug() << "-----------------------------";
    //    for(int i=0;i<albumsModel->rowCount();++i){
    //        QStandardItem *item = albumsModel->item(i);
    //        for(int j=0;j<item->rowCount();++j){
    //           qDebug() << item->child(j)->text();
    //        }
    //        qDebug() << "-----------------------------";
    //    }

        QModelIndex index = this->ui->listViewAlbums->currentIndex();
        QStandardItem *item = albumsModel->item(index.row());
        QString albumId = item->child(ALBUM_ID)->text();
        QString albumName = item->child(ALBUM_NAME)->text();
        QString authorId = item->child(ALBUM_AUTHOR_ID)->text();
        QString authorName = repositoryAuthor->getNameById(authorId.toInt());
        QStandardItemModel *songsForAlbumModel = this->repositoryAlbum->getSongsByAlbumId(albumId.toInt());
        this->setAlbumName(albumName, DEFAULT_ALBUM_LABEL_SPLITTER, authorName);
        this->ui->listViewAlbums->setModel(songsForAlbumModel);
    }
}

void MainWindow::albumNavigationClicked() {
    qDebug() << "Album navigation clicked :)";
    if(this->m_listViewMode == ALBUM_SONGS_MODE) {
        this->m_listViewMode = ALBUM_MODE;
        QStandardItemModel *albumsModel = this->repositoryAlbum->getAll();
        this->setAlbumName();
        this->ui->listViewAlbums->setModel(albumsModel);
    }
}

void MainWindow::addAlbumClicked() {
    qDebug() << "Add album clicked :)";

    QAbstractItemModel *genreModel = this->ui->listViewGenres->model();
    QSqlQueryModel *authorModel = repositoryAuthor->getAll();
    AlbumDialog albumDialog(genreModel, authorModel);

    int dialogResult = albumDialog.exec();
    if(dialogResult == QDialog::Rejected) {
        qDebug() << "Cancel button clicked :(";
        return;
    }

    QStandardItem *albumData = albumDialog.getData();
    if(albumData == NULL) {
        return;
    }


    bool ok;
    //check author
    qDebug() << albumData->child(CALBUM_AUTHOR);
    QString albumAuthorIdOrName = albumData->child(CALBUM_AUTHOR)->text();
    int authorId = albumAuthorIdOrName.toInt(&ok);
    qDebug() << ok;
    if(!ok) {
        authorId = repositoryAuthor->insert(albumAuthorIdOrName);
    }

    //check genre
    qDebug() << albumData->child(CALBUM_GENRE);
    QString albumGenreIdOrName = albumData->child(CALBUM_GENRE)->text();
    int genreId = albumGenreIdOrName.toInt(&ok);
    qDebug() << ok;
    if(!ok) {
        genreId = repositoryGenre->insert(albumGenreIdOrName);
    }

    //insert album
    QString albumName = albumData->child(CALBUM_NAME)->text();
    //TODO: author_id
    //TODO: genre_id
    int releaseYear = albumData->child(CALBUM_RELEASE_YEAR)->text().toInt();
    QString bookletPath = albumData->child(CALBUM_BOOKLET)->text();
    int albumId = repositoryAlbum->insert(albumName, authorId, genreId, releaseYear, bookletPath);
    qDebug() << "albumId:" << albumId;

    //insert songs
    QStandardItem *songs = albumData->child(CALBUM_SONGS);
    qDebug() << "song count:" << songs->rowCount();
    for(int i = 0; i < songs->rowCount(); i ++) {
        qDebug() << "song name:" << songs->child(i)->text();
        repositoryAlbum->insertSongForAlbum(songs->child(i)->text(), albumId, i);
    }

    QStandardItemModel *albumsModel = this->repositoryAlbum->getAll();
    this->ui->listViewAlbums->setModel(albumsModel);
}

void MainWindow::deleteAlbumClicked() {
    qDebug() << "Delete album clicked :)";

    QStandardItemModel *albumsModel = (QStandardItemModel*)this->ui->listViewAlbums->model();
    QModelIndex index = this->ui->listViewAlbums->currentIndex();

    qDebug() << "index:" << index.row();
    QStandardItem *item = albumsModel->item(index.row());
    int albumId = item->child(ALBUM_ID)->text().toInt();
    bool success = this->repositoryAlbum->deleteById(albumId);
    if(success) {
        QStandardItemModel *albumsModel = this->repositoryAlbum->getAll();
        this->setAlbumName();
        this->ui->listViewAlbums->setModel(albumsModel);
        qDebug() << "Set new model with removed row";
    } else {
        qDebug() << "Could not remove genre id: " << albumId;
    }
}

void MainWindow::addGenreClicked() {
    qDebug() << "Add genre clicked :)";

    GenreDialog genreDialog;

    int dialogResult = genreDialog.exec();
    if(dialogResult == QDialog::Rejected) {
        qDebug() << "Cancel button clicked :(";
        return;
    }

    qDebug() << "Add button clicked :)";
    bool success = repositoryGenre->insert(genreDialog.getData());
    if(success) {
        QSqlQueryModel *newModel = this->repositoryGenre->getAll();
        this->ui->listViewGenres->setModel(newModel);
        qDebug() << "Set new model with added row";
    } else {
        qDebug() << "Could not remove " << genreDialog.getData() <<  " genre";
    }
}

void MainWindow::deleteGenreClicked() {
    qDebug() << "Delete genre clicked :)";
    QSqlQueryModel *genresModel = (QSqlQueryModel*)this->ui->listViewGenres->model();
    QModelIndex index = this->ui->listViewGenres->currentIndex();
    qDebug() << "index:" << index.row();
    int genreId = genresModel->record(index.row()).value(GENRE_ID).toInt();
    qDebug() << "genre id:" << genreId;
    bool success = this->repositoryGenre->deleteById(genreId);
    if(success) {
        QSqlQueryModel *newModel = this->repositoryGenre->getAll();
        this->ui->listViewGenres->setModel(newModel);
        qDebug() << "Set new model with removed row";
    } else {
        qDebug() << "Could not remove genre id: " << genreId;
    }
}

void MainWindow::closeClicked() {
    qDebug() << "Close app button clicked :)";
    this->close();
}

void MainWindow::minimizeClicked() {
    qDebug() << "Minimize app button clicked :)";
    this->showMinimized();
}

void MainWindow::maximizeClicked() {
    qDebug() << "Maximize app button clicked :)";
    if(this->m_maximized) {
        this->showNormal();
        this->m_maximized = false;
    } else {
        this->showMaximized();
        this->m_maximized = true;
    }
}

void MainWindow::searchClicked() {
    qDebug("Clicked search :)");
    QString albumNameForSearch = this->ui->lineEditAlbumSearch->text();
    QString authorNameForSearch = this->ui->lineEditAuthorSearch->text();
    QString genreNameForSearch = this->ui->lineEditGenreSearch->text();

    QStandardItemModel *albumsModel = this->repositoryAlbum->searchAll(albumNameForSearch, genreNameForSearch, authorNameForSearch);
    this->ui->listViewAlbums->setModel(albumsModel);
}

void MainWindow::mousePressEvent(QMouseEvent *event) {
    if(!this->m_maximized) {
        m_nMouseClickXCoordinate = event->position().x();
        m_nMouseClickYCoordinate = event->position().y();
    }
}

void MainWindow::mouseMoveEvent(QMouseEvent *event) {
    if(!this->m_maximized) {
        move(event->globalPosition().x()-m_nMouseClickXCoordinate,event->globalPosition().y()-m_nMouseClickYCoordinate);
    } else {
        this->showNormal();
        m_maximized = false;
    }
}

void MainWindow::setAlbumName(QString albumName, QString splitter, QString authorName) {
    this->ui->albumNameSplitter->setText(splitter);
    if(albumName == "" && authorName == "") {
        this->ui->albumNameLabel->setText("");
    } else {
        this->ui->albumNameLabel->setText(albumName + "  |  " + authorName);
    }

}



