
#include "QDebug"
#include "genredialog.h"
#include "ui_genredialog.h"

#include <qgraphicseffect.h>

GenreDialog::GenreDialog(QWidget *parent): QDialog(parent), ui(new Ui::GenreDialog) {
    this->ui->setupUi(this);
    this->setWindowFlags(Qt::WindowType::Dialog | Qt::WindowType::FramelessWindowHint);
    this->setModal(true);

    QGraphicsDropShadowEffect *dropShadow = new QGraphicsDropShadowEffect();
    dropShadow->setColor(QColor(255,255,255));
    dropShadow->setXOffset(2.0);
    dropShadow->setYOffset(2.0);
    dropShadow->setBlurRadius(2);
    this->setGraphicsEffect(dropShadow);

    QObject::connect(this->ui->pushButtonCancel, &QPushButton::clicked, this, &GenreDialog::closeClicked);
    QObject::connect(this->ui->pushButtonAddGenre, &QPushButton::clicked, this, &GenreDialog::addClicked);
}

GenreDialog::~GenreDialog()
{
    delete this->ui;
}

QString GenreDialog::getData() {
    return this->ui->lineEditGenreName->text();
}

void GenreDialog::closeClicked() {
    qDebug() << "Close clicked in dialog :)";
    this->close();
}

void GenreDialog::addClicked() {
    qDebug() << "Add genre clicked in dialog :)";
    this->accept();
}
