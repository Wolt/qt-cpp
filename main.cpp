#include "mainwindow.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QLocale>
#include <QTranslator>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    QApplication::setApplicationName("Album DB");
    QApplication::setApplicationVersion("v1.0");

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addOptions({
        {"create-tables", QCoreApplication::translate("main", "Create database tables from scratch")},
        {"populate-tables", QCoreApplication::translate("main", "Populate database tables with sample data")}
    });

    parser.process(a);

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "QtAlbumStore_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            a.installTranslator(&translator);
            break;
        }
    }

    MainWindow w;
    w.setWindowFlags(Qt::WindowType::Window | Qt::WindowType::FramelessWindowHint | Qt::WindowTitleHint | Qt::CustomizeWindowHint);

    w.setup(
        parser.isSet("create-tables"),
        parser.isSet("populate-tables")
    );

    w.show();
    return a.exec();
}
