#include "repositoryalbum.h"

#include <QSqlError>
#include <qsqlquery.h>
#include "albumcolumn.h"

RepositoryAlbum::RepositoryAlbum() {

}

QStandardItemModel *RepositoryAlbum::getAll() {
    QSqlQuery query;
    if(!query.exec("SELECT id, name, author_id, genre_id, release_year, booklet FROM album")) {
        qDebug() << "Error: RepositoryAlbum::getAll()\n" << query.lastError().text();
        return NULL;
    }

    QStandardItemModel *model = new QStandardItemModel();

    while(query.next()) {
        QStandardItem *item = new QStandardItem(query.value("name").toString());

        item->setChild(ALBUM_ID, new QStandardItem(query.value("id").toString()));
        item->setChild(ALBUM_NAME, new QStandardItem(query.value("name").toString()));
        item->setChild(ALBUM_AUTHOR_ID, new QStandardItem(query.value("author_id").toString()));
        item->setChild(ALBUM_GENRE_ID, new QStandardItem(query.value("genre_id").toString()));
        item->setChild(ALBUM_RELEASE_YEAR, new QStandardItem(query.value("release_year").toString()));

        QString bookletPath = query.value("booklet").toString();
        QPixmap pixmap(bookletPath);
        item->setIcon(pixmap);
        item->setChild(ALBUM_BOOKLET, new QStandardItem(bookletPath));

        model->appendRow(item);
    }

    return model;
}

QStandardItemModel *RepositoryAlbum::getSongsByAlbumId(int albumId) {
    qDebug() << albumId;
    QSqlQuery query;
    query.prepare("SELECT id, name, `order` FROM song WHERE album_id = ?");
    query.bindValue(0, albumId);
    if(!query.exec()) {
        qDebug() << "Error: RepositoryAlbum::getSongsByAlbum()\n" << query.lastError().text();
        return NULL;
    }

    QStandardItemModel *model = new QStandardItemModel();

    while(query.next()) {
        QStandardItem *item = new QStandardItem(query.value("name").toString());

        item->setChild(0, new QStandardItem(query.value("id").toString()));
        item->setChild(1, new QStandardItem(query.value("name").toString()));
        item->setChild(2, new QStandardItem(query.value("order").toString()));

        model->appendRow(item);
    }

    return model;
}

QStandardItemModel *RepositoryAlbum::searchAll(QString albumNameLike, QString genreNameLike, QString authorNameLike) {
    QSqlQuery query;
    bool searchForAlbum = false;
    bool searchForGenre = false;
    bool searchForAuthor = false;
    if(albumNameLike != "") searchForAlbum = true;
    if(genreNameLike != "") searchForGenre = true;
    if(authorNameLike != "") searchForAuthor = true;

    QString queryString = "SELECT a.id, a.name, a.author_id, a.genre_id, a.release_year, a.booklet FROM album AS a";

    if(searchForGenre) queryString += " JOIN genre AS g ON g.id=a.genre_id";
    if(searchForAuthor) queryString += " JOIN author AS au ON au.id=a.author_id";

    if (searchForAlbum || searchForGenre || searchForAuthor) {
        queryString += " WHERE";
    }

    if(searchForAlbum) queryString += " a.name LIKE ?";

    if((searchForAlbum && searchForGenre) || (searchForAlbum && searchForAuthor)) queryString += " AND";

    if(searchForGenre) queryString += " g.name LIKE ?";

    if((searchForGenre && searchForAuthor)) queryString += " AND";

    if(searchForAuthor) queryString += " au.name LIKE ?";

    query.prepare(queryString);
    qDebug() << query.lastQuery();
    qDebug() << "searchForAlbum" << searchForAlbum;
    qDebug() << "searchForGenre" << searchForGenre;
    qDebug() << "searchForAuthor" << searchForAuthor;
    if(searchForAlbum && searchForGenre && searchForAuthor){
        qDebug() << "binding all three";
        query.bindValue(0, "%" + albumNameLike + "%");
        query.bindValue(1, "%" + genreNameLike + "%");
        query.bindValue(2, "%" + authorNameLike + "%");
    } else if(searchForAlbum && searchForGenre) {
        qDebug() << "binding albumNameLike && genreNameLike";
        query.bindValue(0, "%" + albumNameLike + "%");
        query.bindValue(1, "%" + genreNameLike + "%");
    } else if(searchForGenre && searchForAuthor) {
        qDebug() << "binding genreNameLike && authorNameLike";
        query.bindValue(0, "%" + genreNameLike + "%");
        query.bindValue(1, "%" + authorNameLike + "%");
    } else if(searchForAlbum && searchForAuthor) {
        qDebug() << "binding albumNameLike && authorNameLike";
        query.bindValue(0, "%" + albumNameLike + "%");
        query.bindValue(1, "%" + authorNameLike + "%");
    } else if(searchForAlbum) {
        qDebug() << "binding albumNameLike";
        query.bindValue(0, "%" + albumNameLike + "%");
    } else if(searchForGenre) {
        qDebug() << "binding genreNameLike";
        query.bindValue(0, "%" + genreNameLike + "%");
    } else if(searchForAuthor) {
        qDebug() << "binding authorNameLike";
        query.bindValue(0, "%" + authorNameLike + "%");
    }



    if(!query.exec()) {
        qDebug() << query.lastQuery();
        qDebug() << "Error: RepositoryAlbum::searchAll()\n" << query.lastError().text();
        return NULL;
    }

    QStandardItemModel *model = new QStandardItemModel();

    while(query.next()) {
        QStandardItem *item = new QStandardItem(query.value("name").toString());

        item->setChild(ALBUM_ID, new QStandardItem(query.value("id").toString()));
        item->setChild(ALBUM_NAME, new QStandardItem(query.value("name").toString()));
        item->setChild(ALBUM_AUTHOR_ID, new QStandardItem(query.value("author_id").toString()));
        item->setChild(ALBUM_GENRE_ID, new QStandardItem(query.value("genre_id").toString()));
        item->setChild(ALBUM_RELEASE_YEAR, new QStandardItem(query.value("release_year").toString()));

        QString bookletPath = query.value("booklet").toString();
        QPixmap pixmap(bookletPath);
        item->setIcon(pixmap);
        item->setChild(ALBUM_BOOKLET, new QStandardItem(bookletPath));

        model->appendRow(item);
    }

    return model;
}

bool RepositoryAlbum::deleteById(int id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM album WHERE id = ?");
    query.bindValue(0, id);
    if(!query.exec()) {
        qDebug() << "Error: unable to delete from album! \n" << query.lastError().text();
        return false;
    }
    qDebug() << "Info: Deleted album with id: " << id;

    return true;
}

int RepositoryAlbum::insert(QString name, int authorId, int genreId, int releaseYear, QString bookletPath) {
    QSqlQuery query;
    query.prepare("INSERT INTO album (name, author_id, genre_id, release_year, booklet) VALUES (?, ?, ?, ?, ?)");
    query.bindValue(0, name);
    query.bindValue(1, authorId);
    query.bindValue(2, genreId);
    query.bindValue(3, releaseYear);
    query.bindValue(4, bookletPath);
    if(!query.exec()) {
        qDebug() << "Error: unable to insert into album table! \n" << query.lastError().text();
        return -1;
    }
    qDebug() << "Info: Inserted album with id:" << query.lastInsertId().toInt();
    return query.lastInsertId().toInt();
}

int RepositoryAlbum::insertSongForAlbum(QString songName, int albumId, int order) {
    QSqlQuery query;
    query.prepare("INSERT INTO song (name, album_id, `order`) VALUES (?, ?, ?)");
    query.bindValue(0, songName);
    query.bindValue(1, albumId);
    query.bindValue(2, order);
    if(!query.exec()) {
        qDebug() << "Error: unable to insert into song table! \n" << query.lastError().text();
        return -1;
    }
    qDebug() << "Info: Inserted album with id:" << query.lastInsertId().toInt();
    return query.lastInsertId().toInt();
}
