#include "repositorygenre.h"

#include <qsqlquery.h>
#include <qsqlerror.h>

RepositoryGenre::RepositoryGenre() {

}

QSqlQueryModel *RepositoryGenre::getAll() {
    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery("SELECT id, name FROM genre");
    return model;
}

bool RepositoryGenre::deleteById(int id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM genre WHERE id = ?");
    query.bindValue(0, id);
    if(!query.exec()) {
        qDebug() << "Error: unable to create author table! \n" << query.lastError().text();
        return false;
    }
    qDebug() << "Info: Deleted genre with id:" << id;

    return true;
}


int RepositoryGenre::insert(QString name) {
    QSqlQuery query;
    query.prepare("INSERT INTO genre (name) VALUES (?)");
    query.bindValue(0, name);
    if(!query.exec()) {
        qDebug() << "Error: unable to delete from genre table! \n" << query.lastError().text();
        return -1;
    }
    qDebug() << "Info: Inserted genre with id:" << query.lastInsertId().toInt();
    return query.lastInsertId().toInt();
}
