#ifndef GENREDIALOG_H
#define GENREDIALOG_H

#include <qdialog.h>
#include "ui_genredialog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class GenreDialog; }
QT_END_NAMESPACE

class GenreDialog : public QDialog {
    Q_OBJECT
public:
    GenreDialog(QWidget *parent = nullptr);
    ~GenreDialog();
    QString getData();
private:
    Ui::GenreDialog *ui;
private slots:
    void closeClicked();
    void addClicked();
};

#endif // GENREDIALOG_H
