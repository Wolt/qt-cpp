#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "repositoryalbum.h"
#include "repositoryauthor.h"
#include "repositorygenre.h"
#include <genrecolumn.h>
#include <albumcolumn.h>
#include <listviewmode.h>
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    bool setup(bool createTables, bool populateTables);

private:
    Ui::MainWindow *ui;
    RepositoryGenre *repositoryGenre;
    RepositoryAlbum *repositoryAlbum;
    RepositoryAuthor *repositoryAuthor;
    bool setupDatabase(bool createTables, bool populateTables);
    void setupUi();
    LIST_VIEW_MODE m_listViewMode;
    bool m_maximized;
    void setAlbumName(QString albumName = "", QString splitter = "", QString authorName = "");
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    int m_nMouseClickXCoordinate;
    int m_nMouseClickYCoordinate;

private slots:
    void albumDoubleClicked();
    void albumNavigationClicked();
    void addAlbumClicked();
    void deleteAlbumClicked();
    void addGenreClicked();
    void deleteGenreClicked();
    void closeClicked();
    void minimizeClicked();
    void maximizeClicked();
    void searchClicked();
};
#endif // MAINWINDOW_H
