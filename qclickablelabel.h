#ifndef QCLICKABLELABEL_H
#define QCLICKABLELABEL_H

#include <QLabel>

class QClickableLabel : public QLabel {
    Q_OBJECT
public:
    QClickableLabel(QWidget *parent = nullptr);
private:
    void mousePressEvent(QMouseEvent *event);
signals:
    void clicked();
};

#endif // QCLICKABLELABEL_H
