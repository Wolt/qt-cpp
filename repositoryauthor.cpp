#include "repositoryauthor.h"

#include <qsqlquery.h>
#include <qsqlerror.h>

RepositoryAuthor::RepositoryAuthor() {

}

QSqlQueryModel *RepositoryAuthor::getAll() {
    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery("SELECT id, name FROM author");
    return model;
}

int RepositoryAuthor::insert(QString name) {
    QSqlQuery query;
    query.prepare("INSERT INTO author (name) VALUES (?)");
    query.bindValue(0, name);
    if(!query.exec()) {
        qDebug() << "Error: unable to delete from author table! \n" << query.lastError().text();
        return -1;
    }
    qDebug() << "Info: Inserted author with id:" << query.lastInsertId().toInt();
    return query.lastInsertId().toInt();
}

QString RepositoryAuthor::getNameById(int id) {
    QSqlQuery query;
    query.prepare("SELECT name FROM author WHERE id = ?");
    query.bindValue(0, id);
    if(!query.exec()) {
        qDebug() << "Error: RepositoryAuthor::getNameById()\n" << query.lastError().text();
        return NULL;
    }

    while(query.next()){
        return query.value("name").toString();
    }

    return NULL;
}
