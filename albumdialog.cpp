#include "albumdialog.h"
#include "ui_albumdialog.h"

#include <QCompleter>
#include <QFileDialog>
#include <QUuid>
#include <qgraphicseffect.h>

AlbumDialog::AlbumDialog(QAbstractItemModel *genreModel, QAbstractItemModel *authorModel, QWidget *parent) : QDialog(parent), ui(new Ui::AlbumDialog) {
    this->ui->setupUi(this);
    this->setWindowFlags(Qt::WindowType::Dialog | Qt::WindowType::FramelessWindowHint);
    this->setModal(true);
    this->ui->listViewSong->setModel(new QStandardItemModel());

    QCompleter *genreCompleter = new QCompleter(genreModel, this);
    genreCompleter->setCompletionColumn(1);
    genreCompleter->setCompletionMode(QCompleter::PopupCompletion);
    genreCompleter->setModelSorting(QCompleter::UnsortedModel);
    genreCompleter->setFilterMode(Qt::MatchStartsWith);
    genreCompleter->setMaxVisibleItems(5);
    genreCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    QAbstractItemView *genrePopup = genreCompleter->popup();
    genrePopup->setStyleSheet("background-color:#101010;color:#9F9F9F");
    genreCompleter->setPopup(genrePopup);

    QCompleter *authorCompleter = new QCompleter(authorModel, this);
    authorCompleter->setCompletionColumn(1);
    authorCompleter->setCompletionMode(QCompleter::PopupCompletion);
    authorCompleter->setModelSorting(QCompleter::UnsortedModel);
    authorCompleter->setFilterMode(Qt::MatchStartsWith);
    authorCompleter->setMaxVisibleItems(5);
    authorCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    QAbstractItemView *authorPopup = authorCompleter->popup();
    authorPopup->setStyleSheet("background-color:#101010;color:#9F9F9F");
    authorCompleter->setPopup(authorPopup);

    this->ui->lineEditAlbumGenre->setCompleter(genreCompleter);
    this->ui->lineEditAlbumAuthor->setCompleter(authorCompleter);

    QObject::connect(this->ui->pushButtonCancel, &QPushButton::clicked, this, &AlbumDialog::closeClicked);
    QObject::connect(this->ui->pushButtonAddAlbum, &QPushButton::clicked, this, &AlbumDialog::addClicked);
    QObject::connect(this->ui->pushButton_AddSong, &QPushButton::clicked, this, &AlbumDialog::addSongToAlbumClicked);
    QObject::connect(this->ui->pushButtonRemoveSong, &QPushButton::clicked, this, &AlbumDialog::removeSongFromAlbumClicked);
    QObject::connect(this->ui->labelBooklet, &QClickableLabel::clicked, this, &AlbumDialog::bookletClicked);
}

AlbumDialog::~AlbumDialog() {
    delete ui;
}

QStandardItem *AlbumDialog::getData() {
    return this->albumData;
}

void AlbumDialog::closeClicked() {
    qDebug() << "Close clicked in dialog :)";
    this->close();
}

void AlbumDialog::addClicked() {
    qDebug() << "Add album clicked in dialog :)";

    QStandardItem *albumData = new QStandardItem();

    albumData->setChild(CALBUM_NAME, new QStandardItem(this->ui->lineEditAlbumName->text()));

    QString authorName = this->ui->lineEditAlbumAuthor->text();
    QAbstractItemModel *albumAuthorModel = this->ui->lineEditAlbumAuthor->completer()->model();
    bool found = false;
    for (int i = 0; i<albumAuthorModel->rowCount(); i++) {
        if(albumAuthorModel->data(albumAuthorModel->index(i, 1)).value<QString>() == authorName) {
            qDebug() << "found:" << albumAuthorModel->data(albumAuthorModel->index(i, 0)).value<QString>();
            found = true;
            albumData->setChild(CALBUM_AUTHOR, new QStandardItem(albumAuthorModel->data(albumAuthorModel->index(i, 0)).value<QString>()));
            break;
        }
    }
    if(!found) {
        albumData->setChild(CALBUM_AUTHOR, new QStandardItem(authorName));
    }

    QString genreName = this->ui->lineEditAlbumGenre->text();
    QAbstractItemModel *albumGenreModel = this->ui->lineEditAlbumGenre->completer()->model();
    found = false;
    for (int i = 0; i<albumGenreModel->rowCount(); i++) {
        if(albumGenreModel->data(albumGenreModel->index(i, 1)).value<QString>() == genreName) {
            qDebug() << "found:" << albumGenreModel->data(albumGenreModel->index(i, 0)).value<QString>();
            found = true;
            albumData->setChild(CALBUM_GENRE, new QStandardItem(albumGenreModel->data(albumGenreModel->index(i, 0)).value<QString>()));
            break;
        }
    }
    if(!found) {
        albumData->setChild(CALBUM_GENRE, new QStandardItem(genreName));
    }


    albumData->setChild(CALBUM_RELEASE_YEAR, new QStandardItem(this->ui->lineEditAlbumReleaseYear->text()));

    QStandardItem *songStandardItem = new QStandardItem();
    QAbstractItemModel *songModel = this->ui->listViewSong->model();
    for(int i = 0; i<songModel->rowCount(); i++) {
        QString songName = songModel->data(songModel->index(i,0)).value<QString>();
        songStandardItem->setChild(i, new QStandardItem(songName));
    }

    albumData->setChild(CALBUM_SONGS, songStandardItem);

    QDir bookletDir(QCoreApplication::applicationDirPath() + "/images");
    if(!bookletDir.exists()) {
        bookletDir.cdUp();

        bookletDir.mkdir(QCoreApplication::applicationDirPath() + "/images");
    }

    QFile booklet(this->bookletPath);
    QFileInfo bookletFileInfo(this->bookletPath);

    QUuid uuidGen;
    QString storedBookletPath = QCoreApplication::applicationDirPath() + "/images/" + uuidGen.createUuid().toString(QUuid::WithoutBraces) + "." + bookletFileInfo.completeSuffix();
    booklet.copy(storedBookletPath);
    albumData->setChild(CALBUM_BOOKLET, new QStandardItem(storedBookletPath));

    this->albumData = albumData;
    this->accept();
}

void AlbumDialog::addSongToAlbumClicked() {
    qDebug() << "Add song to album clicked in dialog :)";
    QStandardItemModel *model = (QStandardItemModel *)this->ui->listViewSong->model();
    int row = model->rowCount();
    model->insertRow(row, new QStandardItem());
    this->ui->listViewSong->setModel(model);
    QModelIndex index = model->index(row,0);
    ui->listViewSong->setCurrentIndex(index);
    ui->listViewSong->edit(index);
}

void AlbumDialog::removeSongFromAlbumClicked() {
    qDebug() << "Remove song from album clicked in dialog :)";
    QStandardItemModel *model = (QStandardItemModel *)this->ui->listViewSong->model();
    QModelIndex index = this->ui->listViewSong->currentIndex();
    model->removeRow(index.row());
    this->ui->listViewSong->setModel(model);
}

void AlbumDialog::bookletClicked()
{
    QFileDialog fileDialog;
    this->bookletPath = fileDialog.getOpenFileName(this, "Select album booklet", "", "BMP (*.bmp)");
    qDebug() << this->bookletPath;
    if(this->bookletPath == "") {
        this->ui->labelBooklet->setText("Select booklet image");
    } else {
        this->ui->labelBooklet->setPixmap(QPixmap(this->bookletPath));
    }
}
