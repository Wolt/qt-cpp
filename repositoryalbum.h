#ifndef REPOSITORYALBUM_H
#define REPOSITORYALBUM_H

#include <qsqldatabase.h>
#include <qstandarditemmodel.h>

class RepositoryAlbum {
public:
    RepositoryAlbum();
    QStandardItemModel *getAll();
    QStandardItemModel *getSongsByAlbumId(int albumId);
    QStandardItemModel *searchAll(QString albumNameLike, QString genreNameLike, QString authorNameLike);
    bool deleteById(int id);
    int insert(QString name, int authorId, int genreId, int releaseYear, QString bookletPath);
    int insertSongForAlbum(QString songName, int albumId, int order);

};

#endif // REPOSITORYALBUM_H
