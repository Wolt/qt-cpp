# Arguments
App can be launched with two arguments

* --create-tables
  * Generates the database tables
* --populate-tables
  * Populates the generates tables with initial data
