#ifndef ALBUMDIALOG_H
#define ALBUMDIALOG_H

#include <QDialog>
#include <qstandarditemmodel.h>

enum CREATE_ALBUM_COLUMNS {CALBUM_NAME, CALBUM_AUTHOR, CALBUM_GENRE, CALBUM_RELEASE_YEAR, CALBUM_SONGS, CALBUM_BOOKLET};

QT_BEGIN_NAMESPACE
namespace Ui { class AlbumDialog; }
QT_END_NAMESPACE

class AlbumDialog : public QDialog {
    Q_OBJECT

public:
    explicit AlbumDialog(QAbstractItemModel *genreModel, QAbstractItemModel *authorModel, QWidget *parent = nullptr);
    ~AlbumDialog();
    QStandardItem *getData();

private:
    Ui::AlbumDialog *ui;
    QString bookletPath;
    QStandardItem *albumData;
private slots:
    void closeClicked();
    void addClicked();
    void addSongToAlbumClicked();
    void removeSongFromAlbumClicked();
    void bookletClicked();
};

#endif // ALBUMDIALOG_H
