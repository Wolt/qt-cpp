#ifndef SQLLITE_H
#define SQLLITE_H

#include <qsqldatabase.h>

class SqlLiteDatabase {
public:
    SqlLiteDatabase(const QString& path);
    bool createTables();
    bool populateTables();
    const QSqlDatabase &getDatabase() const;

private:
    QSqlDatabase m_database;
};

#endif // SQLLITE_H
