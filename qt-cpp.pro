QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    albumdialog.cpp \
    main.cpp \
    mainwindow.cpp \
    qclickablelabel.cpp \
    repositoryalbum.cpp \
    repositoryauthor.cpp \
    repositorygenre.cpp \
    sqllite.cpp \
    genredialog.cpp

HEADERS += \
    albumcolumn.h \
    albumdialog.h \
    genrecolumn.h \
    listviewmode.h \
    qclickablelabel.h \
    repositoryauthor.h \
    songcolumn.h \
    mainwindow.h \
    repositoryalbum.h \
    repositorygenre.h \
    sqllite.h \
    uinamespace.h \
    genredialog.h

FORMS += \
    albumdialog.ui \
    genredialog.ui \
    mainwindow.ui

TRANSLATIONS += \
    qt-cpp_cs_CZ.ts \
    qt-cpp_en_US.ts
CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    booklet.bmp

# using shell_path() to correct path depending on platform
# escaping quotes and backslashes for file paths
copydata.commands = $(COPY_FILE) \"$$shell_path($$PWD\\booklet.bmp)\" \"$$shell_path($$OUT_PWD)\"
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata
