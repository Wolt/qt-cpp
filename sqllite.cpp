#include "sqllite.h"
#include <stdio.h>
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <qcoreapplication.h>

SqlLiteDatabase::SqlLiteDatabase(const QString &path) {
    m_database = QSqlDatabase::addDatabase("QSQLITE");
    m_database.setDatabaseName(path);

    if (!m_database.open()){
        qDebug() << "Error: connection with database failed!";
        return;
    }

    qDebug() << "Info: connection to database ok ...";
//    QSqlQuery insertGenreQuery;
//    bool success = insertGenreQuery.exec("INSERT INTO genre (name) VALUES ('Rock')");
//    if(!success) {
//        qDebug() << "Error: unable to insert into genre table! \n" << insertGenreQuery.lastError().text();
//        return;
//    }
//    qDebug() << "Info: Populated genre table.";
}

bool SqlLiteDatabase::createTables() {
    qDebug() << "Info: Running database creation table process ...";

    QSqlQuery createAuthorTableQuery;
    bool success = createAuthorTableQuery.exec("CREATE TABLE IF NOT EXISTS author(id integer primary key autoincrement, name text not null)");
    if(!success) {
        qDebug() << "Error: unable to create author table! \n" << createAuthorTableQuery.lastError().text();
        return false;
    }
    qDebug() << "Info: Created author table.";

    QSqlQuery createGenreTableQuery;
    success = createGenreTableQuery.exec("CREATE TABLE IF NOT EXISTS genre(id integer primary key autoincrement, name text not null)");
    if(!success) {
        qDebug() << "Error: unable to create genre table!\n" << createGenreTableQuery.lastError().text();
        return false;
    }
    qDebug() << "Info: Created genre table.";

    QSqlQuery createAlbumTableQuery;
    success = createAlbumTableQuery.exec("CREATE TABLE IF NOT EXISTS album(id integer primary key autoincrement, name text not null, author_id integer not null, genre_id integer not null, release_year integer not null, booklet text null, foreign key (author_id) references author (id), foreign key (genre_id) references genre (id))");
    if(!success) {
        qDebug() << "Error: unable to create author table!\n" << createAlbumTableQuery.lastError().text();
        return false;
    }
    qDebug() << "Info: Created album table.";

    QSqlQuery createSongTableQuery;
    success = createSongTableQuery.exec("CREATE TABLE IF NOT EXISTS song(id integer primary key autoincrement, name text not null, album_id integer not null, \"order\" integer not null, foreign key (album_id) references album (id))");
    if(!success) {
        qDebug() << "Error: unable to create song table!\n" << createSongTableQuery.lastError().text();
        return false;
    }
    qDebug() << "Info: Created song table.";

    QSqlQuery createAuthorToAlbumQuery;
    success = createAuthorToAlbumQuery.exec("CREATE TABLE IF NOT EXISTS author_to_album(author_id integer, album_id integer, primary key (author_id, album_id), foreign key (author_id) references author (id), foreign key (album_id) references album (id))");
    if(!success) {
        qDebug() << "Error: unable to create author_to_album table!\n" << createAuthorToAlbumQuery.lastError().text();
        return false;
    }
    qDebug() << "Info: Created author_to_album table.";

    return true;
}

bool SqlLiteDatabase::populateTables() {
    qDebug() << "Info: Populating database tables with sample data ...";

    QSqlQuery insertAuthorQuery;
    bool success = insertAuthorQuery.exec("INSERT INTO author (name) VALUES ('My Chemical Romance')");
    if(!success) {
        qDebug() << "Error: unable to insert into author table! \n" << insertAuthorQuery.lastError().text();
        return false;
    }
    qDebug() << "Info: Populated author table.";

    QSqlQuery insertGenreQuery;
    success = insertGenreQuery.exec("INSERT INTO genre (name) VALUES ('Rock')");
    if(!success) {
        qDebug() << "Error: unable to insert into genre table! \n" << insertGenreQuery.lastError().text();
        return false;
    }
    qDebug() << "Info: Populated genre table.";

    QSqlQuery insertAlbumQuery;
    success = insertAlbumQuery.exec("INSERT INTO album (name, author_id, genre_id, release_year, booklet) VALUES ('The Black Parade',1,1,2006,'"+QCoreApplication::applicationDirPath()+"/booklet.bmp'), ('May Death Never Stop You',1,1,2014,'"+QCoreApplication::applicationDirPath()+"/booklet.bmp')");
    if(!success) {
        qDebug() << "Error: unable to insert into album table! \n" << insertAlbumQuery.lastError().text();
        return false;
    }
    qDebug() << "Info: Populated album table.";

    QSqlQuery insertSongQuery;
    success = insertSongQuery.exec(
        "INSERT INTO song (name, album_id, \"order\") "
        "VALUES ('The End',1,1),('Dead!',1,2),('This Is How I Disappear',1,3),('The Sharpest Lives',1,4),('Welcome to the Black Parade',1,5),('I Dont Love You',1,6),('House of Wolves',1,7), "
        "('Cancer',1,8),('Mama',1,9),('Sleep',1,10),('Teenagers',1,11),('Disenchanted',1,12),('Famous Last Words',1,13),('Blood',1,14), "
        "('Fake your Death',2,1),('Honey, This Minor Isnt Big Enough for the Two of Us',2,2),('Vampires Will Never Hunt You',2,3),('Helena',2,4),('You know what they do to guys like us in prison',2,5),('Im not okay (I promise)',2,6),('The ghost of you',2,7), "
        "('Welcome to the black parade',2,8),('Cancer',2,9),('Mama',2,10),('Teenagers',2,11),('Famous Last Words',2,12),('Na Na Na',2,13),('SIGN',2,14), "
        "('Planetary',2,15),('The kids from yesterday',2,16),('Skylines and Turnstiles',2,17),('Knives/Sorrow',2,18),('Cubicles',2,19)"
    );
    if(!success) {
        qDebug() << "Error: unable to insert into song table! \n" << insertSongQuery.lastError().text();
        return false;
    }
    qDebug() << "Info: Populated song table.";

    QSqlQuery insertAuthorToAlbumQuery;
    success = insertAuthorToAlbumQuery.exec("INSERT INTO author_to_album (author_id, album_id) VALUES (1,1), (1,2)");
    if(!success) {
        qDebug() << "Error: unable to insert into author_to_album table! \n" << insertAuthorToAlbumQuery.lastError().text();
        return false;
    }
    qDebug() << "Info: Populated author_to_album table.";

    return true;
}

const QSqlDatabase &SqlLiteDatabase::getDatabase() const
{
    return m_database;
}
