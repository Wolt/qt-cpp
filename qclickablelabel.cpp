#include "qclickablelabel.h"

QClickableLabel::QClickableLabel(QWidget *parent): QLabel(parent) {

}

void QClickableLabel::mousePressEvent(QMouseEvent *event) {
    qDebug() << "Clicked QClickableLabel";
    emit clicked();
}
