#ifndef REPOSITORYAUTHOR_H
#define REPOSITORYAUTHOR_H

#include <qsqlquerymodel.h>

class RepositoryAuthor
{
public:
    RepositoryAuthor();
    QSqlQueryModel *getAll();
    int insert(QString name);
    QString getNameById(int id);
};

#endif // REPOSITORYAUTHOR_H
